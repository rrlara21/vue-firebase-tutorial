// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'

Vue.config.productionTip = false

let app;
var config = {
  apiKey: "AIzaSyBvlJcRcH613_yOzDilq4oek-N5jZ4TBpg",
  authDomain: "embody-health-members.firebaseapp.com",
  databaseURL: "https://embody-health-members.firebaseio.com",
  projectId: "embody-health-members",
  storageBucket: "embody-health-members.appspot.com",
  messagingSenderId: "434064709923"
};

firebase.initializeApp(config);
firebase.auth().onAuthStateChanged(function(user) {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      el: '#app',
      template: '<App/>',
      components: { App },
      router
    })
  }
});
